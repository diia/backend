package models;

import io.ebean.Finder;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class User {
    @Id
    @Size(max = 12)
    private String id;
    @Size(max = 20)
    private String name;
    @Size(max = 13)
    private String telephone;
    @Size(max = 20)
    private String paternal_surname;
    @Size(max = 20)
    private String maternal_surname;
    @OneToMany(cascade = CascadeType.ALL)
    public List<TrackLike> track_likes;

    public static Finder<Long, Track> find() {
        return new Finder<>(Track.class);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPaternal_surname() {
        return paternal_surname;
    }

    public void setPaternal_surname(String paternal_surname) {
        this.paternal_surname = paternal_surname;
    }

    public String getMaternal_surname() {
        return maternal_surname;
    }

    public void setMaternal_surname(String maternal_surname) {
        this.maternal_surname = maternal_surname;
    }
}
