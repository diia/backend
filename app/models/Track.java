package models;

import com.typesafe.config.Optional;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.annotation.DbDefault;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Track {
    @Id
    private Long id;
    @Size(max = 30)
    @Column(nullable = false)
    private String track_id;
    @Column(nullable = false)
    private Short source_type;
    @Column(nullable = false)
    private Timestamp timestamp;
    @Column(nullable = false)
    private Short state;
    @Size(max = 100)
    private String track_name;
    @Size(max = 200)
    private String track_artists;
    @Size(max = 200)
    private String track_genres;
    @Size(max = 100)
    private String track_album;
    @Column(columnDefinition = "TEXT")
    private String thumbail_url;
    private Double track_duration_seg;
    @ManyToOne(cascade = CascadeType.ALL)
    private User user;
    @OneToMany(cascade = CascadeType.ALL)
    public List<TrackLike> track_likes;

    public static Finder<Long, Track> find() {
        return new Finder<>(Track.class);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public short getSource_type() {
        return source_type;
    }

    public void setSource_type(short source_type) {
        this.source_type = source_type;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public short getState() {
        return state;
    }

    public void setState(short state) {
        this.state = state;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getTrack_artists() {
        return track_artists;
    }

    public void setTrack_artists(String track_artists) {
        this.track_artists = track_artists;
    }

    public String getTrack_genres() {
        return track_genres;
    }

    public void setTrack_genres(String track_genres) {
        this.track_genres = track_genres;
    }

    public String getTrack_album() {
        return track_album;
    }

    public void setTrack_album(String track_album) {
        this.track_album = track_album;
    }

    public Double getTrack_duration_seg() {
        return track_duration_seg;
    }

    public void setTrack_duration_seg(Double track_duration_seg) {
        this.track_duration_seg = track_duration_seg;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<TrackLike> getTrack_likes() {
        return track_likes;
    }

    public void setTrack_likes(List<TrackLike> track_likes) {
        this.track_likes = track_likes;
    }

    public String getThumbail_url() {
        return thumbail_url;
    }

    public void setThumbail_url(String thumbail_url) {
        this.thumbail_url = thumbail_url;
    }
}
