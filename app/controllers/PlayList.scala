package controllers


import java.sql.Timestamp

import akka.actor.ActorSystem
import akka.stream.Materializer
import io.ebean.Ebean
import javax.inject.{Inject, _}
import models.{Track, TrackLike, User}
import play.api.libs.streams.ActorFlow
import play.api.libs.ws._
import play.api.mvc._

import scala.concurrent.Await
import scala.concurrent.duration._
import play.api.Configuration
import play.api.i18n.{Messages, MessagesApi}
import play.api.Logger

@Singleton
class PlayList @Inject()(implicit system: ActorSystem, materializer: Materializer, ws: WSClient, config: Configuration, messagesApi: MessagesApi) extends Controller {
  val logger: Logger = Logger(this.getClass())

  val spotify_server = config.underlying.getString("spotify.server")
  val spotify_port = config.underlying.getString("spotify.port")
  val spotify_username = config.underlying.getString("spotify.credentials.username")
  val spotify_password = config.underlying.getString("spotify.credentials.password")
  val localmusic_server = config.underlying.getString("localmusic.server")
  val localmusic_port = config.underlying.getString("localmusic.port")
  import scala.concurrent.ExecutionContext.Implicits.global
  var spotify_token = spotify_login()

  case class RESPONSE_DETAIL(info_msg:String)

  import play.api.libs.json.Json._
  import play.api.libs.json._

  object RESPONSE_DETAIL{
    implicit val writter = new Writes[RESPONSE_DETAIL] {
      def writes(http_error: RESPONSE_DETAIL): JsValue =
        Json.toJson(
          obj(
            "details" -> http_error.info_msg
          )
        )
    }
  }

  def spotify_login():String  = {
    Await.result(
      (
        ws.url(s"http://${spotify_server}:${spotify_port}/api/login")
          .withHeaders("content-type" -> "application/x-www-form-urlencoded")
          .post(s"username=${spotify_username}&password=${spotify_password}")
          .recover{case _ => false}
        )
      , 10 seconds) match {
      case response: WSResponse if response.status == 200 => {
        (response.json \ "token").as[String]
      }
      case response: WSResponse if response.status == 400 => "" // Notify user !!!
      case response: WSResponse if response.status == 401 => "" // Notify user !!!
      case _ => {
        // Notify user !!!
        ""
      }
    }
  }

  object validSourceType extends Enumeration {
    sealed case class SourceType private[SourceType](typeShort: Short, typeStr: String) extends Val(typeShort, typeStr)
    val Local = SourceType(1, "local")
    val Spotify = SourceType(2, "spotify")
  }

  object validTrackState extends Enumeration {
    sealed case class TrackState private[TrackState](stateInt: Integer, stateStr: String) extends Val(stateInt, stateStr)
    val unplayed = TrackState(1, "unplayed")
    val played = TrackState(2, "played")
    val skiped = TrackState(3, "skiped")
    val not_found = TrackState(4, "not_found")
  }

  object validLike extends Enumeration {
    sealed case class ValidLike private[ValidLike](stateInt: Short, stateStr: String) extends Val(stateInt, stateStr)
    val like = ValidLike (1, "like")
    val dislike = ValidLike (2, "dislike")
  }

  object validQueryType extends Enumeration {
    sealed case class QueryType private[QueryType](queryType: String) extends Val(queryType)
    val all = QueryType("all")
    val name = QueryType("name")
    val artist = QueryType("artist")
    val genre = QueryType("genre")
    val album = QueryType("album")
  }

  case class AddTrack(track_id: String, source_type: String)

  import scala.concurrent._
  import ExecutionContext.Implicits.global


  def add_track(track_id: String, source_type: String) = Action { implicit request =>
    validSourceType.values.find(_.toString equals source_type) match {
      case None => BadRequest("")
      case Some(source_type) => {
        current_user(request.remoteAddress) match {
          case None => Unauthorized("")
          case Some(user) => {
            source_type match {
              case source_type if source_type == validSourceType.Local => {
                Await.result(
                  (
                    ws.url(s"http://${localmusic_server}:${localmusic_port}/api/get_track_info")
                      .withQueryString("track_id"-> track_id)
                      .get()
                      .recover{case _ => false}
                    )
                  , 10 seconds) match {
                  case response: WSResponse if response.status == 200 => {
                    val new_track = new Track
                    new_track.setTrack_id(track_id)
                    new_track.setSource_type(validSourceType.Local.typeShort)
                    new_track.setTimestamp(new Timestamp(System.currentTimeMillis()))
                    new_track.setState(validTrackState.unplayed.stateInt.shortValue())
                    new_track.setTrack_name((response.json \ "track_name").as[String])
                    new_track.setTrack_artists((response.json \"artists").as[String])
                    new_track.setTrack_genres((response.json \ "genres").as[String])
                    new_track.setTrack_album((response.json \ "album").as[String])
                    new_track.setTrack_duration_seg((response.json \ "duration_seg").as[Double])
                    new_track.setUser(user)
                    Ebean.save(new_track)

                    system.actorSelection("/user/*") ! NewTrack(new_track)

                    Ok("")
                  }
                  case response: WSResponse if response.status == 404 => NotFound("")
                  case _ => {
                    // Notify user !!!
                    ServiceUnavailable("")
                  }
                }

              }
              case source_type if source_type == validSourceType.Spotify => {
                Await.result(
                  (
                    ws.url(s"http://${spotify_server}:${spotify_port}/api/track_info")
                      .withQueryString("track_id"->track_id)
                      .withHeaders("Authorization"->s"Token ${spotify_token}")
                      .get()
                      .recover{case _ => false}
                    )
                  , 10 seconds) match {
                  case response: WSResponse if response.status == 200 => {
                    val track = response.json

                    val new_track = new Track
                    new_track.setTrack_id(track_id)
                    new_track.setSource_type(validSourceType.Spotify.typeShort)
                    new_track.setTimestamp(new Timestamp(System.currentTimeMillis()))
                    new_track.setState(validTrackState.unplayed.stateInt.shortValue())
                    new_track.setTrack_name((track \ "name").as[String])
                    new_track.setTrack_artists(spArtists(track))
                    new_track.setTrack_genres("")
                    new_track.setTrack_album((track \ "album" \ "name").as[String])
                    new_track.setTrack_duration_seg(((track \ "duration_ms").as[Double]/1000))
                    spTrackImage(track, 64) map { i =>
                      new_track.setThumbail_url((i \ "url").as[String])
                    }
                    new_track.setUser(user)
                    Ebean.save(new_track)

                    system.actorSelection("/user/*") ! NewTrack(new_track)

                    Ok("")
                  }
                  case response: WSResponse if response.status == 400 || response.status == 401 => NotFound("")
                  case response: WSResponse if response.status == 401 => {
                    spotify_token = spotify_login()
                    Unauthorized(response.body)
                  }
                  case _ => {
                    // Notify user !!!
                    ServiceUnavailable("")
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  def spArtists(track: JsValue): String = {
    (track \ "artists").as[JsArray].value map { artist =>
      (artist \ "name").as[String]
    } mkString(",")
  }

  def spTrackImage(track: JsValue, size: Int): Option[JsValue] = {
    (track \ "album" \ "images").as[JsArray].value find { image =>
      (image \ "height").as[Int] == size || (image \ "width").as[Int] == size
    }
  }

  def next_track() = Action { implicit request =>
    Ok(Ebean.json.toJson(nexttrack_obj()))
  }

  def nexttrack_obj(): Track = {
    Ebean.find(classOf[Track])
      .where().eq("date(timestamp)", new Timestamp(System.currentTimeMillis()).toLocalDateTime.toLocalDate.toString())
      .and().eq("state", validTrackState.unplayed.stateInt)
      .setMaxRows(1)
      .order().asc("timestamp")
      .fetch("user")
      .fetch("track_likes")
      .findOne()
  }

  def search_track(query_type: String, query: String) = Action { implicit request =>
    validQueryType.withName(query_type) match {
      case validQueryType.all => {
          var localResult: JsValue = JsArray(Seq())
          var spotifyResult: JsValue = JsArray(Seq())

          Await.result(
          (
            ws.url(s"http://${localmusic_server}:${localmusic_port}/api/search_track")
              .withQueryString("type"->"all")
              .withQueryString("query"->query)
              .get()
              .recover{case _ => false}
            )
          , 10 seconds) match {
            case response: WSResponse if response.status == 200 => {
              localResult = response.json
            }
            case response: WSResponse if response.status == 400 => BadRequest(response.body)
            case _ => {
              // Notify user !!!
              ServiceUnavailable("")
            }
          }

          Await.result(
            (
              ws.url(s"http://${spotify_server}:${spotify_port}/api/search_track")
                .withQueryString("type"->"all")
                .withQueryString("query"->query)
                .withHeaders("Authorization"->s"Token ${spotify_token}")
                .get()
                .recover{case _ => false}
              )
            , 10 seconds) match {
            case response: WSResponse if response.status == 200 => {
              spotifyResult = response.json
            }
            case response: WSResponse if response.status == 400 => BadRequest(response.body)
            case response: WSResponse if response.status == 401 => {
              spotify_token = spotify_login()
              Unauthorized(response.body)
            }
            case _ => {
              // Notify user !!!
              ServiceUnavailable("")
            }
          }

          Ok(JsObject(Seq(
            "local"->localResult,
            "spotify"->spotifyResult
          )))
      }
      case validQueryType.name => Ok("")
      case validQueryType.artist => Ok("")
      case validQueryType.album => Ok("")
      case validQueryType.genre => Ok("")
      case _ => {
        BadRequest("")
      }
    }
  }

  def set_like_state(id_track: Long, like_type: String) = Action { implicit request =>
    val messages: Messages = messagesApi.preferred(request)

    (current_user(request.remoteAddress)) match {
      case (None) => Unauthorized("")
      case Some(user) => {
        validLike.values.find(_.toString equals like_type) match {
          case None => BadRequest("")
          case Some(new_state) => {
            val track = Ebean.find(classOf[Track], id_track)

            Option(track) match {
              case None => {
                NotFound(Json.toJson(RESPONSE_DETAIL(messages("error.track_notfound"))))
              }
              case Some(track) => {

                val trackLike = Ebean.find(classOf[TrackLike])
                  // a track is available only by day ...
                  .where().eq("user.id", user.getId())
                  .where().eq("track.id", track.getId())
                  .setMaxRows(1)
                  .fetch("track")
                  .fetch("user")
                  .fetch("track.track_likes")
                  .findOne()

                Option(trackLike) match {
                  case Some(tl) => {
                    if (tl.getState != new_state) {
                      tl.setState(new_state.id.asInstanceOf[Short])
                      Ebean.save(tl)
                      system.actorSelection("/user/*") ! NewTrackLike(tl)
                    }
                  }
                  case None => {
                    val newTrackLike = new TrackLike
                    newTrackLike.setUser(user)
                    newTrackLike.setTrack(track)
                    newTrackLike.setTimestamp(new Timestamp(System.currentTimeMillis()))
                    newTrackLike.setState(new_state.id.asInstanceOf[Short])
                    Ebean.save(newTrackLike)
                    system.actorSelection("/user/*") ! NewTrackLike(newTrackLike)
                  }
                }
                Ok("")
              }
            }
          }
        }
      }
    }
    Ok("")
  }



  def clear() = Action { implicit request =>
    import io.ebean.Ebean
    var down = Ebean.createSqlUpdate("DELETE FROM track_like WHERE date(timestamp) = date(\":current_date\")")
      .setParameter("current_date", new Timestamp(System.currentTimeMillis()))
    down.execute()
    down = Ebean.createSqlUpdate("DELETE FROM track WHERE date(timestamp) = date(\":current_date\")")
        .setParameter("current_date", new Timestamp(System.currentTimeMillis()))
    down.execute()
    Ok("")
  }

  def set_track_state(id_track: Long, state: String) = Action { implicit request =>
    val messages: Messages = messagesApi.preferred(request)

    validTrackState.values.find(
      validState => {
        validState.toString equals state
      }
    ) match {
      case None => BadRequest("")
      case Some(trackState) => {
        val track = Ebean.find(classOf[Track], id_track)

        Option(track) map {
          t => {
            t.setState(trackState.id.shortValue())
            Ebean.save(t)
            Ok("")
          }
        }getOrElse{
          NotFound(Json.toJson(RESPONSE_DETAIL(messages("error.track_notfound"))))
        }
      }
    }

  }

  def current_user(remoteAddress: String): Option[User] = {
    import scala.sys.process._
    val arpTable = "arp -an" !!

    val pattern = """([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}).*(([0-9A-Fa-f]{2}[:-]){5}[0-9A-Fa-f]{2})""".r
    var ipMacTable = Seq[(String,String)]()

    pattern.findAllIn(arpTable).matchData foreach {
      m => ipMacTable = ipMacTable :+ (m.group(1), m.group(2))
    }

    ipMacTable.find(_._1==remoteAddress) match {
      case None => {
        None
      }
      case Some(ipM) => {
        val macAddress = ipM._2.replace(":", "").trim()
        import io.ebean.Ebean
        Option(Ebean.find(classOf[User], macAddress)) map {
          user => Some(user)
        }getOrElse{
          val new_user = new User
          new_user.setId(macAddress)
          new_user.setName("User "+macAddress.filter(_.isLetter))
          new_user.setTelephone("")
          new_user.setPaternal_surname("")
          new_user.setMaternal_surname("")
          Ebean.save(new_user)
          Some(new_user)
        }
      }
    }

  }

  import akka.actor._

  case class Player()
  case class NewTrackLike(track: TrackLike)
  case class NewTrack(track_like: Track)
  case class TrackPlayed(id_track: Long)
  var current_track:Long = 0;

  object PlayListWS {
    def props(out: ActorRef, user: Any) = Props(new PlayListWS(out, user))
  }

  class PlayListWS(out: ActorRef, user: Any) extends Actor {

    def receive = {
      case js:JsValue => {
        (js \ "status").validate[String] map { status =>
          status match {
            case "track_status" => {

              ( Some((js \ "track_id").as[Int]), Some((js \ "new_status").as[String])) match {
                case (Some(track_id), Some(new_status)) => {
                  val track = Ebean.find(classOf[Track], track_id)
                  val ns = validTrackState.withName(new_status)

                  Option(track) map {
                    t => {
                      t.setState(ns.id.shortValue())
                      Ebean.save(t)
                    }
                  }
                }
                case _ =>
              }
              system.actorSelection("/user/*") ! TrackPlayed(0)
              out !  Json.parse(
                s"""{
                  "next_track":${Ebean.json().toJson(nexttrack_obj())}
                }""")
            }
            case "track_playing" => {
              (js \ "track_id").validate[Long] map { track_id =>
                current_track = track_id
                system.actorSelection("/user/*") ! TrackPlayed(track_id)
              }
            }
            case _ =>
          }
        }
      }

      case TrackPlayed(track_id) => {
        user match {
          case user: User => {

            out ! Json.parse(
              s"""{
                              "track_playing":${current_track}
                          }"""
            )
          }
          case _ =>
        }
      }

      case NewTrack(nt) => {
        user match {
          case user: User => {
            out !  Json.parse(
              s"""{
                  "new_track":${Ebean.json().toJson(nt)}
                  }"""
            )
          }
          case player: Player => {
            out !  Json.parse(
              s"""{
                  "next_track":${Ebean.json().toJson(nexttrack_obj())}
                  }"""
            )
          }
          case _ =>
        }
      }

      case NewTrackLike(ntl) => {
        out !  Json.parse(
                  s"""{
                  "new_track_like":${Ebean.json().toJson(ntl)}
                  }"""
                )
      }

      case _ => Json.parse(
                 s"""{
                  "error":"Unsupported command"
                  }"""
                )
    }

    override def preStart(): Unit = {
      super.preStart()

      user match {
        case user: User => {
            val tracks = Ebean.find(classOf[Track])
              .where().eq("date(timestamp)", new Timestamp(System.currentTimeMillis()).toLocalDateTime.toLocalDate.toString())
              .order().asc("timestamp")
              .fetch("user") // ensure eager loading
              .fetch("track_likes")
              .findList()

            out ! Json.parse(
              s"""{
            "all_tracks":${Ebean.json().toJson(tracks)}
            }""")

            out ! Json.parse(
              s"""{
            "personal_data":${Ebean.json().toJson(user)}
            }""")

            out ! Json.parse(
                            s"""{
                                "track_playing":${current_track}
                            }""")
        }
        case player: Player => {
            out ! Json.parse(
              s"""{
            "next_track":${Ebean.json().toJson(nexttrack_obj())}
            }""")
        }
        case _ =>
      }

    }

  }

  import play.api.mvc.WebSocket.MessageFlowTransformer
  implicit val messageFlowTransformer = MessageFlowTransformer.jsonMessageFlowTransformer[JsValue, JsValue]


  def web_socket() = WebSocket.acceptOrResult[JsValue, JsValue] { request =>
    Future.successful(
      request match {
        /*
        case req if(req.headers.headers.find(_._1.equals("token"))) => {
        }
        */
        case _ => {
          current_user(request.remoteAddress) match {
            case Some(user) => Right(ActorFlow.actorRef(out => PlayListWS.props(out, user)))
            case _ => Left(Forbidden)
          }
        }
      }
    )
  }

  def socket_player() = WebSocket.acceptOrResult[JsValue, JsValue] { request =>
    Future.successful(
      request match {
        case req if(isLocalAddress(req)) => {
          Right(ActorFlow.actorRef(out => PlayListWS.props(out, Player())))
        }
        case _ =>  Left(Forbidden)
      }
    )
  }

  def isLocalAddress(request: RequestHeader): Boolean = {
    import scala.sys.process._
    var command_result = """echo IyEvYmluL3NoCgppcCAtbyBhZGRyIHwgYXdrICchL15bMC05XSo6ID9sb3xsaW5rXC9ldGhlci8ge2dzdWIoIi8iLCAiICIpOyBwcmludCAiIyIkNH0nCg==""" #| "base64 -d" #| "sh" !!

    val ips = command_result.trim.replaceAll("[\n\r]", "").split("#")
    request.remoteAddress match {
      case ip:String if (ips.contains(ip)) => true
      case "localhost" => true
      case "127.0.0.1" => true
      case "0:0:0:0:0:0:0:0" => true
      case _ => false
    }
  }

  def personal_data() = Action { implicit request =>
    current_user(request.remoteAddress) match {
      case Some(user) => {
        request.body.asJson match {
          case Some(new_data) => {
            new_data.as[Map[String, String]].foreach( field => {
              if(field._1 equals "name"){
                user.setName(field._2)
              }else if(field._1 equals "maternal_surname"){
                user.setMaternal_surname(field._2)
              }else if(field._1 equals "paternal_surname"){
                user.setPaternal_surname(field._2)
              }else if(field._1 equals "telephone"){
                user.setTelephone(field._2)
              }
            })
            Ebean.save(user)
            Ok("")
          }
          case None => BadRequest("")
        }
      }
      case None => Unauthorized("")
    }
  }
}