logLevel := Level.Warn

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.9")

// https://github.com/playframework/play-ebean
addSbtPlugin("com.typesafe.sbt" % "sbt-play-ebean" % "3.2.0")