name := "playlistbackend"
 
version := "1.0" 
      
lazy val `playlistbackend` = (project in file(".")).enablePlugins(PlayJava)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
scalaVersion := "2.11.11"

libraryDependencies ++= Seq( javaJdbc , cache , ws, filters )

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

lazy val myProject = (project in file(".")).enablePlugins(PlayJava, PlayScala, PlayEbean)

// https://mvnrepository.com/artifact/org.mariadb.jdbc/mariadb-java-client
libraryDependencies += "org.mariadb.jdbc" % "mariadb-java-client" % "2.3.0"